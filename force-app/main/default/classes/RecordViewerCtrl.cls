public class RecordViewerCtrl {
    
    @AuraEnabled
    public static List<Object> getObjectApiNames(){
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        List<Object> options = new List<Object>();
        for(String objName : gd.keySet()){
            Schema.DescribeSObjectResult objResult = gd.get(objName).getDescribe();
            if(objResult.isAccessible() && !objResult.isCustomSetting() && objResult.isQueryable()){
                options.add(new Map<String,String>{'label'=>objResult.getLabel(),'value'=>objResult.getName()});
            }
        }
        System.debug('changes Test44');
        return options;
    }
    
    @AuraEnabled
    public static Map<String,object> getRecords(String objName,Integer rowSize){
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objName);
        FieldSelection__mdt fieldsRecord = FieldSelection__mdt.getInstance(objName);
        List<String> queryFields = new List<String>{};
        List<Object> columns = new List<Object>();
        Set<String> fields = new Set<String>();
        if(fieldsRecord != null){
            fields.addAll(fieldsRecord.Fields__c.split(','));
        }else{
            fields.add('Name');
            fields.add('CreatedDate');
            fields.add('CreatedById');
        }
        Map<String,Schema.SObjectField> allFields = objType.getDescribe().fields.getMap();
        for(String field : fields){
            if(!allFields.containsKey(field)) continue;
            Schema.DescribeFieldResult dfr = allFields.get(field).getDescribe();
            if(!dfr.isAccessible()) continue;
            Map<String,Object> column = new Map<String,Object>{
                'editable' => dfr.isUpdateable(),
                'label'=> field =='CreatedById'?'CreatedBy':dfr.getLabel(),
                'fieldName' => field =='CreatedById'?'CreatedBy':dfr.getName(),
                'type' => getType(dfr.getType())
            };
            columns.add(column);
            queryFields.add(field =='CreatedById'?'CreatedBy.Name':field);
        }
        if(queryFields.isEmpty()) throw new AuraHandledException('No Column to Show');
        queryFields.add('Id'); // added id to query for edit purpose
        String query = 'SELECT '+String.join(queryFields,',')+' FROM '+objName+' ORDER BY Createddate desc limit '+rowSize;
        return new Map<String,object>{'data'=>Database.query(query),'columns'=>columns};
    }
    
    @AuraEnabled
    public static void updateRecords(List<Sobject> records){
        try{
            update records;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    private static String getType(Schema.DisplayType sdt){
        if(sdt == Schema.DisplayType.CURRENCY ){
            return 'currency';
        }
        if(sdt == Schema.DisplayType.PERCENT ){
            return 'percent';
        }
        if(sdt == Schema.DisplayType.LONG || sdt == Schema.DisplayType.DOUBLE || sdt == Schema.DisplayType.INTEGER){
            return 'number'; 
        }
        if(sdt == Schema.DisplayType.PHONE){
            return 'phone'; 
        }
        if(sdt == Schema.DisplayType.Boolean){
            return 'boolean'; 
        }
        if(sdt == Schema.DisplayType.DATE){
            return 'date-local'; 
        } 
        if(sdt == Schema.DisplayType.DATETIME){
            return 'date'; 
        }
        if(sdt == Schema.DisplayType.EMAIL){
            return 'email'; 
        }
        if(sdt == Schema.DisplayType.URL){
            return 'url'; 
        }
        return 'text'; 
    }
}